class API {
  static String SERVER_IP_Adress = '192.168.31.22';

  static String SERVER_PORT = '8000';

  static String BASE_URL =
      'http://' + SERVER_IP_Adress + ':' + SERVER_PORT + '/';

  static String LOGIN = 'login/';

  static String HEAD_IMAGE = 'head_image/';

  static String CHANGE_INFO = 'change_info/';

  static String POST_CASE = 'post_illcase/';

  static String GET_PHOTO = 'get_photo/';

  static String GET_SEG_PHOTO_LIST = 'get_seg_photo_list/';

  static String GET_SEG_PHOTO = 'get_seg_photo/';

  static String TYPE_USER_LIST = 'user_list/';

  static String POST_PUSHCASE = 'post_pushcase/';

  static String ILL_Case = 'illcase/';

  static String GET_PUSHCASE = 'pushcase/';

  static String DELETE_PUSHCASE = 'delete_pushcase/';

  static String CHANGE_SUG = 'put_illcase/';

  static String DELETE_CASE = 'del_illcase/';

  String explain = '这是访问存储路径的类';
}
