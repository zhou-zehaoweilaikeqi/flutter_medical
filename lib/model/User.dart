// ignore_for_file: sort_constructors_first

import "dart:io";
import 'package:flutter/material.dart';
import 'package:path/path.dart';

import '../api/Api.dart';
import 'package:sqflite/sqflite.dart';

class User {
  static String job_num = '无';
  static String name = '无';
  static String password = '无';
  static String phone_num = '无';
  static String hospital = '无';
  static int type = -1; // -1 表示没有设置
  static String head_image = '无';

  String explain = "用户实体类";

  static setUser(Map map) {
    User.job_num = map['job_num'];
    User.name = map['name'];
    User.password = map['password'];
    User.phone_num = map['phone_num'];
    User.hospital = map['hospital'];
    User.type = map['type'];
    User.head_image = map['head_image'];
  }

  static Map<String, Object> toMap() {
    return {
      'job_num': job_num,
      'name': name,
      'password': password,
      'phone_num': phone_num,
      'hospital': hospital,
      'type': type,
      'head_image': head_image,
    };
  }

  static String getUserHeadImageUrl() {
    return API.BASE_URL + API.HEAD_IMAGE + User.job_num;
  }

  //todo: 与数据库同步的函数
  static Future<bool> updateToLocalDB() async {
    final database = openDatabase(
      join(await getDatabasesPath(), 'medical_pro_db.db'),
      version: 1,
    );
    final db = await database;
    try {
      WidgetsFlutterBinding.ensureInitialized();

      await db.insert(
        'userinfo',
        User.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      db.close();
      return true;
    } catch (e) {
      print(e);
      db.close();
      return false;
    }
  }
}

void main() {
  print(User.job_num);
  User.job_num = '222';
  print(User.job_num);
}
