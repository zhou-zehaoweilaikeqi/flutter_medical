import "dart:io";
import 'package:flutter/material.dart';
import 'package:path/path.dart';

import '../api/Api.dart';
import 'package:sqflite/sqflite.dart';

class IllCase {
  int? case_id;
  String? patient_name;
  String? patient_age;
  String? patient_idcard_num;
  String? phone_num;
  String? allergy_history;
  String? habits_str;
  String? date;
  String? uploder;
  String? result;

  List<String>? image_names;
  List<String>? expert_sug;

  IllCase(
      {case_id,
      patient_name,
      patient_age,
      patient_idcard_num,
      phone_num,
      allergy_history,
      habits_str,
      date,
      uploder,
      result,
      image_names,
      expert_sug});

  factory IllCase.fromJson(Map<String, dynamic> json) {
    return IllCase(
      case_id: json['case_id'],
      patient_name: json['patient_name'],
      patient_age: json['patient_age'],
      patient_idcard_num: json['patient_idcard_num'],
      phone_num: json['phone_num'],
      allergy_history: json['allergy_history'],
      habits_str: json['habits_str'],
      date: json['date'],
      uploder: json['uploder'],
      result: json['result'],
      image_names: json['image_names'],
      expert_sug: json['expert_sug'],
    );
  }

  int? get caseid => this.case_id;

  set caseid(int? value) => this.case_id = value;

  get patientname => this.patient_name;

  set patientname(value) => this.patient_name = value;

  get patientage => this.patient_age;

  set patientage(value) => this.patient_age = value;

  get patientidcard_num => this.patient_idcard_num;

  set patientidcard_num(value) => this.patient_idcard_num = value;

  get phonenum => this.phone_num;

  set phonenum(value) => this.phone_num = value;

  get allergyhistory => this.allergy_history;

  set allergyhistory(value) => this.allergy_history = value;

  get habitsstr => this.habits_str;

  set habitsstr(value) => this.habits_str = value;

  get getDate => this.date;

  set setDate(date) => this.date = date;

  get getUploder => this.uploder;

  set setUploder(uploder) => this.uploder = uploder;

  get getResult => this.result;

  set setResult(result) => this.result = result;

  get imagenames => this.image_names;

  set imagenames(value) => this.image_names = value;

  get expertsug => this.expert_sug;

  set expertsug(value) => this.expert_sug = value;
}
