import 'package:flutter/material.dart';
import '../../pages/illcase/CaseFirstPage.dart';
import '../../pages/illcase/CaseSecondPage.dart';
import '../../pages/illcase/PostCaseFBPage.dart';
import '../../pages/illcase/ShowCasePage.dart';
import '../../pages/illcase/ShowPushCasePage.dart';
import '../../pages/list/ExpertList.dart';
import '../../pages/message/message_list_page.dart';
import '../../pages/mine/common_problem.dart';
import '../../pages/mine/feed_back.dart';
import '../../pages/mine/file_upload_download.dart';
import '../../pages/mine/user_detail.dart';
import '../../splash.dart';
import '../../pages/expect_consult/Expect_detail.dart';
import '../../pages/login/login.dart';
import '../../pages/login/user_agreement.dart';
import '../../pages/message/message_detail_page.dart';
import '../../pages/mine/account_security.dart';
import '../../pages/mine/chart_demo.dart';
import '../../pages/mine/system_settings.dart';
import '../../pages/toolbar.dart';

/// 路由
class GlobalRouter {
  /// 路由
  /// 从非toolbar页面（子页面）跳转到toolbar页面（主页）实现：
  /// pushName到对应的路由，因为Toolbar是单例模式，所以只会创建一个
  /// pushName之后，在ToolBar，initState中获取当前的路由，实现切换页面
  static final _routes = {
    /// 过渡页面
    '/': (BuildContext context, {Object? args}) => const SplashPage(),

    /// 主页面
    '/home': (BuildContext context, {Object? args}) => const Toolbar(),

    /// 登录页面
    '/loginPage': (BuildContext context, {Object? args}) => const Login(),

    /// 用户协议页面
    '/user_agreement': (BuildContext context, {Object? args}) =>
        const UserAgreement(),

    /// 用户详情
    '/user_detail': (BuildContext context, {Object? args}) =>
        const UserDetail(),

    /// 常见问题
    '/common_problem': (BuildContext context, {Object? args}) =>
        const CommonProblem(),

    /// 意见反馈
    '/feed_back': (BuildContext context, {Object? args}) => const FeedBack(),

    /// 修改密码
    '/account_security': (BuildContext context, {Object? args}) =>
        const AccountSecurity(),

    /// 系统设置
    '/system_settings': (BuildContext context, {Object? args}) =>
        const SystemSettings(),

    /// 图片识别和检测
    '/chart_demo': (BuildContext context, {Object? args}) => const ChartDemo(),

    /// 文件上传与下载
    '/file_upload_download': (BuildContext context, {Object? args}) =>
        const FileUploadDownload(),

    ///专家问诊详情
    '/expect_detail': (BuildContext context, {Object? args}) =>
        const ExpectDetail(),

    /// 信息列表
    '/message_list_page': (BuildContext context, {Object? args}) =>
        const MessageListPage(),

    /// 信息详情
    '/message_detail_page': (BuildContext context, {Object? args}) =>
        const MessageDetailPage(),

    '/inputcase1': (BuildContext context, {Object? args}) => CaseFirstPage(),
    '/inputcase2': (BuildContext context, {arguments}) =>
        CaseSecondPage(arguments: arguments),
    '/post_case': (BuildContext context, {arguments}) =>
        PostCaseFBPage(arguments: arguments),
    '/expert_list': (BuildContext context, {Object? args}) => ExpertListPage(),
    '/show_case_page': (BuildContext context, {arguments}) =>
        ShowCasePage(arguments),
    '/show_push_case_page': (context, {arguments}) =>
        ShowPushCasePage(arguments),
  };

  static GlobalRouter? _singleton;

  GlobalRouter._internal();

  factory GlobalRouter() {
    return _singleton ?? GlobalRouter._internal();
  }

  /// 监听route
  var onGenerateRoute = (RouteSettings settings) {
    // 统一处理
    final String? name = settings.name;
    final Function? pageContentBuilder = _routes[name];
    if (pageContentBuilder != null) {
      if (settings.arguments != null) {
        final Route route = MaterialPageRoute(
            builder: (context) =>
                pageContentBuilder(context, arguments: settings.arguments));
        return route;
      } else {
        final Route route = MaterialPageRoute(
            builder: (context) => pageContentBuilder(context));
        return route;
      }
    }
  };
}
