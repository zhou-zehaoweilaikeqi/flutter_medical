import 'package:flutter/material.dart';

class ExpertOrNotWidget extends StatefulWidget {
  String? arguments;
  ExpertOrNotWidget({this.arguments});

  @override
  _ExpertOrNotWidgetState createState() =>
      _ExpertOrNotWidgetState(arguments: arguments);
}

class _ExpertOrNotWidgetState extends State<ExpertOrNotWidget> {
  String? arguments;
  _ExpertOrNotWidgetState({this.arguments});
  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    print('专家工号:$arguments');
    if (arguments == '') {
      return Divider();
    } else {
      return Text('选择的专家工号：$arguments');
    }
  }
}
