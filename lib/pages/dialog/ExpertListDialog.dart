import 'package:flutter/material.dart';
import '../list/ExpertList.dart';

// 自定义Dialog需要继承Dialog类

class ExpertListDialog extends Dialog {
  @override
  Widget build(BuildContext context) {
    print('context：$context');
    return Material(
        type: MaterialType.transparency,
        child: Container(
            height: 300,
            width: 300,
            color: Colors.white,
            child: ExpertListPage()));
  }
}
