import 'package:flutter/material.dart';

// 自定义Dialog需要继承Dialog类

class ImageDialog extends Dialog {
  late String imageUrl;

  ImageDialog(this.imageUrl);

  @override
  Widget build(BuildContext context) {
    print('context：$context');
    return Material(
        type: MaterialType.transparency,
        child: Center(
            child: Image.network(
          imageUrl,
        )));
  }
}
