import 'package:flutter/material.dart';
import '/core/widget/common_widget.dart';
import '/model/user_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '/core/utils/shared_preferences_util.dart';
import '/core/utils/string_util.dart';
import '/core/widget/custom_app_bar.dart';

/// 我的页面
class Mine extends StatefulWidget {
  const Mine({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MineState();
}

class _MineState extends State<Mine> {
  /// 用户信息
  UserModel userModel = UserModel();

  @override
  void initState() {
    super.initState();
    sharedGetData("loginInfo", List).then((data) {
      if (StringUtil.isNotEmpty(data)) {
        List<String> loginInfo = data as List<String>;
        setState(() {
          userModel = UserModel(
            deptId: '11111',
            deptName: '新医大附一院',
            phone: '13258965478',
          );
          userModel.nickName = loginInfo[2];
          userModel.avatar = loginInfo[3];
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(
          title: '我的',
          backgroundColor: const Color.fromRGBO(91, 149, 255, 0.9),
          titleColor: Colors.white,
          borderBottom: false),
      body: Container(
        decoration:
            const BoxDecoration(color: Color.fromRGBO(240, 240, 240, 1)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            /// 头部
            topWidget(),
            Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      /// 常见问题
                      commonProblem(),

                      /// 意见反馈
                      feedback(),

                      /// 账号安全
                      accountSecurity(),

                      /// 图表统计
                      chartCal(),

                      /// 文件上传与下载
                      fileUplodAndDownload(),

                      /// 系统设置
                      systemSettings(),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }

  /// 头部
  Widget topWidget() {
    return Container(
      height: 130.w,
      decoration: const BoxDecoration(color: Color.fromRGBO(91, 149, 255, 0.9)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// 头像 + 名字
          InkWell(
            onTap: () {
              Get.toNamed("/user_detail", arguments: userModel);
            },
            child: Container(
              margin: EdgeInsets.only(left: 30.w),
              child: ListTile(
                contentPadding: EdgeInsets.only(left: -25.w, right: -25.w),
                leading: CommonWidget.imageWidget(
                    imageUrl: userModel.avatar,
                    hasDefault: true,
                    width: 100.w,
                    height: 100.w),
                title: Text(
                  userModel.nickName ?? "",
                  style: TextStyle(color: Colors.white, fontSize: 50.sp),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// 常见问题
  Widget commonProblem() {
    return CommonWidget.simpleWidgetWithMine(
        title: '常见问题',
        icon: const Icon(Icons.report_gmailerrorred_outlined,
            color: Color.fromRGBO(255, 148, 6, 1)),
        callBack: () {
          Get.toNamed("/common_problem");
        });
  }

  /// 意见反馈
  Widget feedback() {
    return CommonWidget.simpleWidgetWithMine(
        title: '意见反馈',
        icon: const Icon(Icons.headset_mic_outlined,
            color: Color.fromRGBO(95, 154, 255, 1)),
        callBack: () {
          Get.toNamed("/feed_back");
        });
  }

  /// 账号安全
  Widget accountSecurity() {
    return CommonWidget.simpleWidgetWithMine(
        title: '账号安全',
        icon: const Icon(Icons.security_outlined,
            color: Color.fromRGBO(49, 110, 222, 1)),
        callBack: () {
          /// 子页面带返回值的
          Get.toNamed("/account_security")
              ?.then((value) => {print("新密码：$value")});
        });
  }

  /// 系统设置
  Widget systemSettings() {
    return Container(
      padding: EdgeInsets.only(top: 20.w),
      child: CommonWidget.simpleWidgetWithMine(
          title: '系统设置',
          icon: const Icon(Icons.settings,
              color: Color.fromRGBO(39, 103, 220, 1)),
          callBack: () {
            Get.toNamed("/system_settings");
          }),
    );
  }

  /// 图表
  Widget chartCal() {
    return Container(
      padding: EdgeInsets.only(top: 20.w),
      child: CommonWidget.simpleWidgetWithMine(
          title: '图片检测',
          icon: const Icon(Icons.table_chart_outlined,
              color: Color.fromRGBO(79, 203, 116, 1)),
          callBack: () {
            Get.toNamed("/chart_demo");
          }),
    );
  }

  /// 文件上传下载测试
  Widget fileUplodAndDownload() {
    return Container(
      padding: EdgeInsets.only(top: 20.w),
      child: CommonWidget.simpleWidgetWithMine(
          title: '文件上传与下载',
          icon: const Icon(Icons.cloud_sync_outlined,
              color: Color.fromRGBO(242, 100, 124, 1)),
          callBack: () {
            Get.toNamed("/file_upload_download");
          }),
    );
  }
}
