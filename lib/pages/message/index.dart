import 'package:flutter/material.dart';
import '/core/utils/toast.dart';
import '/core/widget/common_widget.dart';
import '/core/widget/custom_app_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

/// 消息页面
class Message extends StatefulWidget {
  const Message({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MessageState();
}

class _MessageState extends State<Message> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(title: '消息'),
      body: Container(
        decoration:
            const BoxDecoration(color: Color.fromRGBO(240, 240, 240, 1)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            CommonWidget.searchWidget(callBack: (val) {
              ToastUtils.toast("点击了搜索：$val");
            }),
            Expanded(
              flex: 1,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: listMessage(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static List<Widget> listMessage() {
    List<Widget> list = [];

    /// 系统消息
    list.add(CommonWidget.listTile(
        title: "系统消息",
        leading: Icon(
          Icons.campaign_outlined,
          color: const Color.fromRGBO(228, 246, 241, 1),
          size: 50.w,
        ),
        leadingBackgroundColor: const Color.fromRGBO(88, 202, 147, 1),
        callBack: () {
          Get.toNamed("/message_list_page");
        }));

    /// 医院动态
    list.add(CommonWidget.listTile(
        title: "医院动态",
        leading: Icon(
          Icons.newspaper_outlined,
          color: const Color.fromRGBO(228, 246, 241, 1),
          size: 50.w,
        ),
        leadingBackgroundColor: const Color.fromRGBO(79, 119, 237, 1),
        callBack: () {
          Get.toNamed("/message_list_page");
        }));

    /// 健康资讯
    list.add(CommonWidget.listTile(
        title: "健康资讯",
        leading: Icon(
          Icons.message_outlined,
          color: const Color.fromRGBO(228, 246, 241, 1),
          size: 50.w,
        ),
        leadingBackgroundColor: const Color.fromRGBO(243, 86, 67, 1),
        callBack: () {
          Get.toNamed("/message_list_page");
        }));

    return list;
  }
}
