import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '/api/Api.dart';
import '/core/utils/StringUtils.dart';

//https://github.com/liusilong/Flutter_Movie

class ExpertListPage extends StatefulWidget {
  final String? title;

  ExpertListPage({Key? key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ExpertListPageState();
  }
}

class _ExpertListPageState extends State<ExpertListPage> {
  List subjects = [];
  String title = '题目';

  @override
  void initState() {
    Fluttertoast.showToast(
      msg: '点击一位专家推送',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 3,
      backgroundColor: Colors.white,
      textColor: Colors.black,
      fontSize: 16.0,
    );
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: getBody(),
    );
  }

  loadData() async {
    String loadRUL = API.BASE_URL + API.TYPE_USER_LIST + '1';
    http.Response response = await http.get(Uri.parse(loadRUL));
    var result = json.decode(utf8.decode(response.bodyBytes));

    print(result);
    setState(() {
      // title = result['title'];
      // print('title: $title');
      subjects = result;
    });
  }

  getItem(var subject) {
    var row = Container(
        margin: EdgeInsets.all(4.0),
        child: GestureDetector(
          onTap: () {
            print('点击了一个${subject['name']}');
            var expert_map = {
              "job_num": subject['job_num'],
              "name": subject['name']
            };
            // Navigator.pop(context);
            Navigator.of(context).pop(expert_map);
          },
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child: Image.network(
                  StringUtils.getHeadImageUrl(subject['job_num']),
                  width: 100.0,
                  height: 100.0,
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 8.0),
                height: 150.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
//                    医生名称
                    Text(
                      subject['name'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                      ),
                      maxLines: 1,
                    ),
//                    所属医院：
                    Text(
                      '所属医院：${subject['hospital']}',
                      style: TextStyle(fontSize: 16.0),
                    ),
//                    类型
                    Text("联系电话：${subject['phone_num']}"),
                  ],
                ),
              ))
            ],
          ),
        ));
    return Card(
      child: row,
    );
  }

  getBody() {
    if (subjects.length != 0) {
      return ListView.builder(
          itemCount: subjects.length,
          itemBuilder: (BuildContext context, int position) {
            return getItem(subjects[position]);
          });
    } else {
      // 加载菊花
      return CupertinoActivityIndicator();
    }
  }
}
