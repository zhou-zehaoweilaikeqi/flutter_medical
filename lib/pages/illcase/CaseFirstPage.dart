import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '/core/utils/StringUtils.dart';

class CaseFirstPage extends StatefulWidget {
  CaseFirstPage({Key? key}) : super(key: key);

  @override
  _CaseFirstPageState createState() => _CaseFirstPageState();
}

class _CaseFirstPageState extends State<CaseFirstPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController idController = TextEditingController();
  TextEditingController phonenumController = TextEditingController();
  TextEditingController ageController = TextEditingController();

  bool? cb_drink_value;
  bool? cb_smoke_value;
  bool? cb_stayup_value;

  bool? history;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cb_drink_value = false;
    cb_smoke_value = false;
    cb_stayup_value = false;
    history = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('基本信息'),
        ),
        body: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.all(20),
            padding: const EdgeInsets.all(5),
            child: Column(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.all(10),
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: '姓名'),
                    )),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: TextField(
                    controller: idController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), labelText: '身份证号'),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    controller: phonenumController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), labelText: '联系电话'),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    controller: ageController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), labelText: '年龄'),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Text('习惯'),
                      Divider(),
                      Checkbox(
                          value: this.cb_smoke_value,
                          onChanged: (v) {
                            setState(() {
                              this.cb_smoke_value = v!;
                            });
                          }),
                      Text('抽烟'),
                      Checkbox(
                          value: this.cb_stayup_value,
                          onChanged: (v) {
                            setState(() {
                              this.cb_stayup_value = v!;
                            });
                          }),
                      Text('熬夜'),
                      Checkbox(
                          value: this.cb_drink_value,
                          onChanged: (v) {
                            setState(() {
                              this.cb_drink_value = v!;
                            });
                          }),
                      Text('喝酒'),
                    ],
                  ),
                ),
                Container(
                    margin: const EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Text('有无过敏史：'),
                        // Radio是多选一的组件，groupvalue一致的为一组选项，每个选项的value值要不一样
                        // 二选一value可以用布尔值，多选一可以用数字
                        Radio(
                            value: true,
                            groupValue: this.history,
                            onChanged: (v) {
                              setState(() {
                                this.history = v as bool?;
                                print(this.history);
                              });
                            }),
                        Text('有'),
                        Radio(
                            value: false,
                            groupValue: this.history,
                            onChanged: (v) {
                              setState(() {
                                this.history = v as bool?;
                                print(this.history);
                              });
                            }),
                        Text('无'),
                      ],
                    )),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: ElevatedButton(
                      onPressed: () => {
                            if (!StringUtils.isName(nameController.text.trim()))
                              {
                                Fluttertoast.showToast(
                                  msg: '姓名输入有误',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0,
                                )
                              }
                            else if (!StringUtils.isCardId(
                                idController.text.trim()))
                              {
                                Fluttertoast.showToast(
                                  msg: '身份证号输入有误',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0,
                                )
                              }
                            else if (!StringUtils.isPhone(
                                phonenumController.text.trim()))
                              {
                                Fluttertoast.showToast(
                                  msg: '手机号输入有误',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0,
                                )
                              }
                            else if (!StringUtils.isAge(
                                ageController.text.trim()))
                              {
                                Fluttertoast.showToast(
                                  msg: '年龄输入有误',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0,
                                )
                              }
                            else
                              {
                                Navigator.pushNamed(context, '/inputcase2',
                                    arguments: {
                                      'name': nameController.text.trim(),
                                      'idNum': idController.text.trim(),
                                      'phoneNum':
                                          phonenumController.text.trim(),
                                      'age': ageController.text.trim(),
                                      'drink': cb_drink_value,
                                      'smoke': cb_smoke_value,
                                      'stayup': cb_stayup_value,
                                      'history': history,
                                    })
                              }
                          },
                      child: Text('下一步')),
                ),
              ],
            ),
          ),
        ));
  }
}
