import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import '/core/widget/common_widget.dart';
import '/core/widget/custom_app_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '/config/constant.dart';
import '/core/widget/nk_swiper_pagination.dart';

///工作台页面
class WorkBench extends StatefulWidget {
  const WorkBench({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WorkBenchState();
}

class _WorkBenchState extends State<WorkBench> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(title: '工作台'),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
              left: Constant.paddingLeftRight,
              right: Constant.paddingLeftRight),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              rotationChart(),
              commonFunctions(),
              ExpectOffice(),
            ],
          ),
        ),
      ),
    );
  }

  /// 轮播图
  static Widget rotationChart() {
    List<String> imageList = [
      "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zx123.cn%2FResources%2Fzx123cn%2Fuploadfile%2F2016%2F1110%2F3623c1091d9530a0e9f322ab11a7d07b.jpg&refer=http%3A%2F%2Fimg.zx123.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1648806126&t=8e2c07128a4ee8b9251c9f98eff7f193",
      "https://gimg2.baidu.com/image_search/src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20170910%2F3e5c10d7052643ab9382f4e701579ccb.jpeg&refer=http%3A%2F%2F5b0988e595225.cdn.sohucs.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1649325782&t=33adb3762394b5f42b77282c2d58f407",
      "https://bkimg.cdn.bcebos.com/pic/9e3df8dcd100baa1d233d02b4410b912c8fc2e06?x-bce-process=image/watermark,image_d2F0ZXIvYmFpa2U4MA==,g_7,xp_5,yp_5/format,f_auto",
    ];
    return SizedBox(
      height: 300.w,
      child: Swiper(
        itemBuilder: (context, index) {
          // 配置图片地址
          return Image.network(
            imageList[index],
            fit: BoxFit.fill,
          );
        },
        // 配置图片数量
        itemCount: imageList.length,
        // 底部分页器
        pagination: NKSwiperPagination(),
        // 无限循环
        loop: true,
        // 自动轮播
        autoplay: true,
      ),
    );
  }

  /// 常用功能
  Widget commonFunctions() {
    List<Widget> list = [];

    /// 快速检测
    list.add(CommonWidget.workBenchIcon(
      title: "快速检测",
      backgroundColor: const Color.fromRGBO(239, 142, 53, 1),
      icon: Icon(
        Icons.holiday_village_outlined,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
      callBack: todo,
    ));

    /// 检测报告
    list.add(CommonWidget.workBenchIcon(
      title: "检测报告",
      backgroundColor: const Color.fromRGBO(239, 142, 53, 1),
      icon: Icon(
        Icons.local_activity,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
    ));

    /// 检查预约
    list.add(CommonWidget.workBenchIcon(
      title: "检查预约",
      backgroundColor: const Color.fromRGBO(79, 119, 237, 1),
      icon: Icon(
        Icons.book_outlined,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
    ));

    /// 在线建档
    list.add(CommonWidget.workBenchIcon(
      title: "在线建档",
      backgroundColor: const Color.fromRGBO(88, 202, 147, 1),
      icon: Icon(
        Icons.airplane_ticket_outlined,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
    ));

    /// 添加功能
    list.add(CommonWidget.workBenchIcon(
      title: "添加功能",
      backgroundColor: const Color.fromRGBO(230, 230, 230, 1),
      icon: Icon(
        Icons.add,
        color: const Color.fromRGBO(144, 148, 157, 1),
        size: 60.w,
      ),
    ));
    return SizedBox(
      height: 500.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 20.w, bottom: 20.w),
            child: const Text(
              "核心功能",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w800),
            ),
          ),
          Wrap(
            spacing: 80.w,
            children: list,
          )
        ],
      ),
    );
  }

  /// 专家推荐
  static Widget ExpectOffice() {
    List<Widget> list = [];

    /// 预约挂号
    list.add(CommonWidget.workBenchIcon(
      title: "预约挂号",
      backgroundColor: const Color.fromRGBO(79, 119, 237, 1),
      icon: Icon(
        Icons.edit_note_outlined,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
    ));

    /// 普通门诊
    list.add(CommonWidget.workBenchIcon(
      title: "普通门诊",
      backgroundColor: const Color.fromRGBO(88, 202, 147, 1),
      icon: Icon(
        Icons.access_time_filled_outlined,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
    ));

    /// 专家门诊
    list.add(CommonWidget.workBenchIcon(
      title: "专家门诊",
      backgroundColor: const Color.fromRGBO(239, 142, 53, 1),
      icon: Icon(
        Icons.handshake_outlined,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 60.w,
      ),
    ));

    /// 快速门诊
    list.add(CommonWidget.workBenchIcon(
      title: "快速门诊",
      backgroundColor: const Color.fromRGBO(79, 119, 237, 1),
      icon: Icon(
        Icons.timer,
        color: const Color.fromRGBO(228, 246, 241, 1),
        size: 50.w,
      ),
    ));

    return SizedBox(
      height: 500.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 20.w),
            child: const Text("专家推荐",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w800)),
          ),
          Wrap(
            spacing: 80.w,
            children: list,
          )
        ],
      ),
    );
  }

  todo() {
    Navigator.pushNamed(context, '/inputcase1');
  }
}
