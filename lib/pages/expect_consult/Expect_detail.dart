import 'package:flutter/material.dart';
import '/config/constant.dart';
import '/core/widget/common_widget.dart';
import '/model/user_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import '/core/utils/toast.dart';
import '/core/widget/custom_app_bar.dart';

/// 通讯里详情
class ExpectDetail extends StatefulWidget {
  const ExpectDetail({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ExpectDetailState();
}

class _ExpectDetailState extends State<ExpectDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppbar(context: context, title: '详情', borderBottom: false),
      body: const Center(
        child: Text("这里是详细的病历单"),
      ),
    );
  }
}
