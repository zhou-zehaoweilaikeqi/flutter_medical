import 'package:flutter/material.dart';
import '/core/widget/common_widget.dart';
import '/core/widget/custom_app_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

/// 专家咨询
class ConsultPage extends StatefulWidget {
  const ConsultPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ConsultPageState();
}

class _ConsultPageState extends State<ConsultPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: customAppbar(title: '专家咨询'),
        body: Center(
          child: GestureDetector(
            child: Text("这是一个未完成的专家回复界面"),
            onTap: () {
              Get.toNamed("/expect_detail");
            },
          ),
        ));
  }
}
