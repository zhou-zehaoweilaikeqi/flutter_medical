import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../core/utils/shared_preferences_util.dart';
import '../../core/utils/toast.dart';
import '../../core/widget/common_widget.dart';
import '../../core/widget/custom_app_bar.dart';

/// 登录页面
class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey _formKey = GlobalKey<FormState>();
  // 点击空白  关闭键盘 时传的一个对象
  FocusNode blankNode = FocusNode();
  String _account = 'admin';
  String _password = 'admin123';
  bool _isObscure = true;
  Color _eyeColor = Colors.grey;
  Color _accountBorderColor = Colors.white;
  Color _passwordBorderColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: customAppbar(borderBottom: false),
        body: InkWell(
          onTap: () => closeKeyboard(context),
          child: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 40.w, right: 40.w),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    /// logo
                    Container(
                      height: 200,
                      padding: const EdgeInsets.only(
                        left: 30,
                        right: 30,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.3),
                          image: const DecorationImage(
                            image:
                                AssetImage('assets/images/login_page_bg.jpg'),
                            alignment: Alignment.center,
                            fit: BoxFit.fill,
                          )),
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                              width: double.infinity,
                              padding: const EdgeInsets.symmetric(
                                vertical: 20,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                color: Colors.white.withOpacity(0.6),
                              ),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/images/logo.png',
                                    width: 60,
                                    height: 60,
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Text(
                                    '新医大附一院',
                                    style: TextStyle(
                                      color: Colors.blue,
                                      fontSize: 30,
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),

                    /// 账号
                    buildAccountTextField(),

                    /// 密码
                    buildPasswordTextField(),

                    /// 登录
                    buildLoginButton(),

                    /// 协议
                    Container(
                      margin: EdgeInsets.only(top: 30.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          const Text(
                            '账号由系统管理员统一发方，申请账号请联系管理员',
                            style: TextStyle(
                                fontSize: 13,
                                color: Color.fromRGBO(153, 153, 153, 1)),
                          ),
                          InkWell(
                            child: const Text(
                              '详情请点击《服务条款》',
                              style: TextStyle(
                                  color: Color.fromRGBO(85, 122, 157, 1),
                                  fontSize: 13),
                            ),
                            onTap: () => Get.toNamed("/user_agreement"),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: <Color>[
                                    Colors.blue.withOpacity(0.2),
                                    Colors.blue,
                                  ],
                                  begin: FractionalOffset(0.0, 0.0),
                                  end: FractionalOffset(1.0, 1.0),
                                  stops: <double>[0.0, 1.0],
                                  tileMode: TileMode.clamp),
                            ),
                            width: 100.0,
                            height: 1.0,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 15.0, right: 15.0),
                            child: Text(
                              '©XJU',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 16.0,
                                  fontFamily: 'WorkSansMedium'),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: <Color>[
                                    Colors.blue,
                                    Colors.blue.withOpacity(0.2),
                                  ],
                                  begin: FractionalOffset(0.0, 0.0),
                                  end: FractionalOffset(1.0, 1.0),
                                  stops: <double>[0.0, 1.0],
                                  tileMode: TileMode.clamp),
                            ),
                            width: 100.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  ///账号
  Container buildAccountTextField() {
    return Container(
      padding: EdgeInsets.only(top: 55.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("账号",
              style:
                  TextStyle(fontSize: 32.sp, color: const Color(0xff737A83))),
          Container(
            height: 90.w,
            padding: EdgeInsets.only(left: 40.w),
            margin: EdgeInsets.only(top: 24.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.w),
                color: const Color(0xffEEEFF2),
                border: Border.all(color: _accountBorderColor, width: 1.w)),
            child: TextFormField(
                initialValue: _account,
                style:
                    TextStyle(fontSize: 32.sp, color: const Color(0xff051220)),
                decoration: InputDecoration(
                  // 表单提示信息
                  hintText: "请输入账号",
                  hintStyle: TextStyle(
                      fontSize: 32.sp, color: const Color(0xff737A83)),
                  // 取消自带的下边框
                  border: InputBorder.none,
                ),
                onSaved: (String? value) => {
                      _account = value!,
                    },
                onChanged: (String value) => _account = value,
                onTap: () {
                  setState(() {
                    _passwordBorderColor = Colors.white;
                    _accountBorderColor = const Color(0xff2692F0);
                  });
                },
                inputFormatters: [
                  LengthLimitingTextInputFormatter(50),
                ]),
          ),
        ],
      ),
    );
  }

  ///密码
  Container buildPasswordTextField() {
    return Container(
      padding: EdgeInsets.only(top: 34.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "密码",
            style: TextStyle(fontSize: 32.sp, color: const Color(0xff737A83)),
          ),
          Container(
              height: 90.w,
              padding: EdgeInsets.only(left: 40.w),
              margin: EdgeInsets.only(top: 24.w),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.w),
                  color: const Color(0xffEEEFF2),
                  border: Border.all(color: _passwordBorderColor, width: 1.w)),
              child: Row(
                children: [
                  SizedBox(
                    width: 450.w,
                    child: TextFormField(
                        initialValue: _password,
                        style: TextStyle(
                            fontSize: 32.sp, color: const Color(0xff051220)),
                        decoration: InputDecoration(
                          // 表单提示信息
                          hintText: "请输入密码",
                          hintStyle: TextStyle(
                              fontSize: 32.sp, color: const Color(0xff737A83)),
                          // 取消自带的下边框
                          border: InputBorder.none,
                        ),
                        obscureText: _isObscure,
                        onSaved: (String? value) => _password = value!,
                        onChanged: (String value) => _password = value,
                        onTap: () {
                          setState(() {
                            _accountBorderColor = Colors.white;
                            _passwordBorderColor = const Color(0xff2692F0);
                          });
                        },
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(50),
                        ]),
                  ),
                  const Expanded(child: Text("")),
                  IconButton(
                      iconSize: 30.w,
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: _eyeColor,
                      ),
                      onPressed: () {
                        setState(() {
                          _eyeColor = _isObscure ? Colors.blue : Colors.grey;
                          _isObscure = !_isObscure;
                        });
                      })
                ],
              )),
        ],
      ),
    );
  }

  ///登录按钮
  SizedBox buildLoginButton() {
    return SizedBox(
      width: 630.w,
      child: CommonWidget.buttonWidget(
          title: '登录',
          padding: const EdgeInsets.only(left: 0, right: 0),
          callBack: () {
            if ((_formKey.currentState as FormState).validate()) {
              onSubmit(context);
            }
          }),
    );
  }

  /// 点击空白  关闭输入键盘
  void closeKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(blankNode);
  }

  ///执行提交方法
  void onSubmit(BuildContext context) {
    closeKeyboard(context);
    if (_account.trim().isEmpty) {
      ToastUtils.toast("账号不能为空");
    } else if (_password.trim().isEmpty) {
      ToastUtils.toast("密码不能为空");
    } else {
      Map<String, dynamic> data = {
        'username': _account.trim(),
        'password': base64Encode(utf8.encode(_password.trim().trim())),
        'rememberMe': 'true',
        'ismoble': 'ismoble'
      };
      List<String> loginInfo;
      //写死了这部分，后期数据库连接
      if (data['username'] == 'admin' &&
          data['password'] == base64Encode(utf8.encode('admin123'))) {
        loginInfo = [
          data['username'],
          data['password'],
          '管理员',
          'https://desk-fd.zol-img.com.cn/t_s960x600c5/g5/M00/02/03/ChMkJlbKxo2IT63KACN7OztCegEAALHmwPZIQ0AI3tT786.jpg'
        ];
        sharedAddAndUpdate("loginInfo", List, loginInfo); //把登录信息保存到本地
        Get.offNamed("/home");
      } else {
        ToastUtils.toast('用户名或密码错误');
      }
    }
  }
}
